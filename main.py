# coding: utf-8

import sys
import re
import os
import qdarkstyle

from qtpy.QtWidgets import QApplication, QMainWindow
from qtpy.QtGui     import QPixmap, QIcon

from qdarkstyle.dark.palette  import DarkPalette
from qdarkstyle.light.palette import LightPalette

from library.functions import *
from ui.ui_mainwindow  import Ui_MainWindow

appname  = "SyncSubtitle"

about    = appname + " version 1.0\n\nThis program synchronize subtitle \
files\naccording to the audio in a video"
    
authors  = ["Luis Acevedo", "<laar@pm.me>"]

credits_ = ["https://kushtej.github.io/subsync"]

license_ = "Copyright 2020. All code is copyrighted by the respective authors.\n" \
+ appname + " can be redistributed and/or modified under the terms of \
the GNU GPL versions 3 or by any future license endorsed by " + authors[0] + \
".\nThis program is distributed in the hope that it will be useful, but \
WITHOUT ANY WARRANTY; without even the implied warranty of \
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
    
third_party = "App logo - Icons by Orion Icon Library - https://orioniconlibrary.com"

# Resources path
tmp_path1 = "ui/resources/img/"
tmp_path2 = "SyncSubtitle.AppDir/usr/bin/ui/resources/img/"
path      = str()

if(os.path.exists(tmp_path1)):
    path = tmp_path1
else:
    path = tmp_path2

subtitle_file_names = list() # List of subtitles to sync
completed           = 0      # Used in progress bar

class MainWindow(QMainWindow, Ui_MainWindow):
    """
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
    """
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.connectSignalsSlots()

    def connectSignalsSlots(self):
        self.progressBar.setValue(0) # Progress bar
        
        # Search files button
        pixmap = QIcon(path+"Start-Menu-Search-icon.png")
        self.btn_search.setIcon(pixmap)
        self.btn_search.clicked.connect(self.search)

        # Sync subtitles button
        pixmap = QIcon(path+"Accept-icon.png")
        self.btn_accept.setIcon(pixmap)
        self.btn_accept.clicked.connect(self.synchronization)

        # Clear file list button
        pixmap = QIcon(path+"Actions-edit-clear-locationbar-rtl-icon.png")
        self.btn_clear.setIcon(pixmap)
        self.btn_clear.clicked.connect(self.clear)

        # About
        self.actionAbout.triggered.connect(self.about_)
        
        # About Qt
        self.actionAbout_Qt.triggered.connect(self.aboutQt)
        
        # Authors
        self.actionAuthors.triggered.connect(self.authors_)
        
        # License
        self.actionLicense.triggered.connect(self.license_)

        # Third party
        self.actionthird_party.triggered.connect(self.third_party_)

        # Change theme
        self.btn_change_theme.setStyleSheet(
            "QPushButton { background-color: purple; } \
                QPushButton::hover { \
                background-color: grey; \
            }"
        )
        self.btn_change_theme.setCheckable(True)
        #self.btn_change_theme.setChecked(True)
        self.btn_change_theme.clicked.connect(self.toggle_theme)

        # Exit
        self.btn_exit.clicked.connect(self.exit)

    # Search subtitle files in file system
    def search(self):
        aux = list()

        dialog = QtWidgets.QFileDialog(self)
        dialog.setFileMode(QtWidgets.QFileDialog.AnyFile)

        dialog.setNameFilter("File (*.srt)")
        global subtitle_file_names
        aux = subtitle_file_names

        dialog.setViewMode(QtWidgets.QFileDialog.Detail)

        """
        # Select one file
        if dialog.exec_():
            aux.append(dialog.selectedFiles()[0]) # Add selected subtitle to file list to sync

        self.subtitle_file_list.setText(list_to_string(subtitle_file_names)) # Show selected subtitle file
        """

        #Select multiple files
        select_multiple_files(dialog)
        if dialog.exec():
            for i in dialog.selectedFiles():
                aux.append(i) # Add selected subtitle to files list to sync

        self.subtitle_file_list.setText(list_to_string(aux)) # Show selected subtitle files

    # sync selected subtitle files
    def synchronization(self):
        sub_time_m   = self.spinBox_sub_m.value()
        sub_time_s   = self.spinBox_sub_s.value()
        audio_time_m = self.spinBox_audio_m.value()
        audio_time_s = self.spinBox_audio_s.value()

        sync_time = synctime(sub_time_m, sub_time_s, audio_time_m, audio_time_s)

        #calibrate(filename, output_name, sync_time)
        
        tmp = QtWidgets.QFileDialog.getSaveFileName(self, ("Save F:xile"), "SELECT DESTINATION FOLDER",)
        tmp = tmp[0].split("/")

        if len(tmp) > 1:
            save_path = ""
            for i in tmp[1:-1]:
                save_path += i + "/"
            save_path = "/" + save_path
            
            aux = list()

            global completed
            completed = completed
            completed = 0

            main_function = calibrate
            global subtitle_file_names
            aux = subtitle_file_names

            if len(aux) == 0:
                QtWidgets.QMessageBox.critical(self, "Error", "You must have selected at least one file")
            else:
                self.label_status.setText("Sycing...")
                button_reply = QtWidgets.QMessageBox.question(self, "Confirm", "Proceed")
                if button_reply == QtWidgets.QMessageBox.Yes:
                    self.progressBar.setValue(5)
                    for i in range(len(aux)):
                        #try:
                            tmp         = aux[i].split("/")
                            output_name = save_path + tmp[-1]
                            
                            main_function(aux[i], output_name, sync_time)
                            
                            completed = self.update_progress_bar(len(aux), completed)
                        #except:
                            #QtWidgets.QMessageBox.critical(self, "Error", "An Error happened duing the synchronization.\n" + "The file: " + "aux" + "\nCould be corrupted or damaged")
                    self.label_status.setText("Ready")
                    QtWidgets.QMessageBox.about(self, "Done", "synchronization success")
                else:
                    self.label_status.setText("Ready")

    # Clear selected file list
    def clear(self):
        self.subtitle_file_list.setText("")
        subtitle_file_names.clear()
        self.progressBar.setValue(0)
        self.spinBox_sub_m.setValue(0)
        self.spinBox_sub_s.setValue(0)
        self.spinBox_audio_m.setValue(0)
        self.spinBox_audio_s.setValue(0)

    def update_progress_bar(self, max_, completed):
        increment = 100 / max_

        if completed < 100:
            completed += increment
            self.progressBar.setValue(completed)
        
        return completed
    
    def about_(self):
        QtWidgets.QMessageBox.about(self, "About", about)

    def aboutQt(self):
        QtWidgets.QMessageBox.aboutQt(self)
        
    def authors_(self):
        text = "Authors:\n" + authors[0] + " " + authors[1] + "\n\n\n" + "Credits:\n" + credits_[0] + "\n"
        QtWidgets.QMessageBox.about(self, "Authors", text)
        
    def license_(self):
        QtWidgets.QMessageBox.about(self, "License", license_)

    def third_party_(self):
        QtWidgets.QMessageBox.about(self, "Third party", third_party)

    def toggle_theme(self):
        if not self.btn_change_theme.isChecked():
            app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api="pyside2", palette=DarkPalette))
        else:
            app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api="pyside2", palette=LightPalette))

    def exit(self):
        sys.exit()

if __name__ == "__main__":
    
    print("\n" + appname + " Copyright (C) 2022 " + authors[0] + ".\nEste programa viene con ABSOLUTAMENTE NINGUNA GARANTÍA.\nEsto es software libre, y le invitamos a redistribuirlo\nbajo ciertas condiciones.\nPor favor, leer el archivo README.")

    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api="pyside2", palette=DarkPalette))

    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
