import sys
import re

from qtpy import QtWidgets

def synctime(sub_start_min, sub_start_sec, video_start_min, video_start_sec):
    import datetime as dt

    a = dt.datetime(100,1,1,0,video_start_min,video_start_sec)
    b = dt.datetime(100,1,1,0,sub_start_min,sub_start_sec)

    return int((a-b).total_seconds())


def haste_delay(time,sec): 
    import datetime
    a = datetime.datetime(100,1,1,int(time[0]),int(time[1]),int(time[2]))
    b = a + datetime.timedelta(seconds=sec)
    return b.time()


def calibrate(filename, output_name, sync_time):
    name = filename
    sec  = sync_time

    f=open(name,"r", encoding="latin")
    #f1=open("sync_"+name,"w")
    f1=open(output_name + "_synced.srt", "w")

    for line in f:
        all_line=line.split()
        if('-->'in all_line):

                #start_time
                start_time=all_line[0].split(":")
                start_time_end=start_time[2].split(",")[1]
                start_time=[re.sub(',.*$', '', i) for i in start_time]
                hasted_delayed_start_time=haste_delay(start_time,sec)

                #end_time
                end_time=all_line[2].split(":")
                end_time_end=end_time[2].split(",")[1]
                end_time=[re.sub(',.*$', '', i) for i in end_time]
                hasted_delayed_end_time=haste_delay(end_time,sec)

                #writing to file
                f1.write(str(hasted_delayed_start_time)+','+str(start_time_end)+' --> '+str(hasted_delayed_end_time)+','+str(end_time_end))
                f1.write("\n")

        else:
            f1.write(line)

# Convert selected file list_ to string
# in order to shown them in the window
def list_to_string(list_):
    string = str()
    for i in range(len(list_)):
        string += str(i+1) + " - " + list_[i] + "\n\n"
    return string

# Select multiple files
def select_multiple_files(dialog):
    file_view = dialog.findChild(QtWidgets.QListView, "ListView")
    if file_view:
        file_view.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
    f_tree_view = dialog.findChild(QtWidgets.QTreeView)
    if f_tree_view:
        f_tree_view.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)

    return f_tree_view
